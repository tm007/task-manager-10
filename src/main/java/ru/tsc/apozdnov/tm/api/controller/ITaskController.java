package ru.tsc.apozdnov.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void clearTasks();

    void createTask();

}
