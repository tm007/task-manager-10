package ru.tsc.apozdnov.tm.api.repository;

import ru.tsc.apozdnov.tm.model.Project;
import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    Project create(String name, String description);

    void remove(Project task);

    Project add(Project task);

    List<Project> findAll();

    void clear();

}
