package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.model.Project;
import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    void remove(Project project);

    Project add(Project project);

    List<Project> findAll();

    void clear();

}
