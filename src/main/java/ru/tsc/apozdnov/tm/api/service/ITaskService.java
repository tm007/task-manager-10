package ru.tsc.apozdnov.tm.api.service;

import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name);

    Task create(String name, String description);

    void remove(Task task);

    Task add(Task task);

    List<Task> findAll();

    void clear();

}
