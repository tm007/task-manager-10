package ru.tsc.apozdnov.tm.controller;

import ru.tsc.apozdnov.tm.api.controller.IProjectController;
import ru.tsc.apozdnov.tm.api.service.IProjectService;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.util.TerminalUtil;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjectList() {
        System.out.println("**** PROJECTS LIST ****");
        final List<Project> projects = projectService.findAll();
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }
        System.out.println("**** OK ****");
    }

    @Override
    public void clearProjects() {
        System.out.println("**** PROJECT CLEAR ****");
        projectService.clear();
        System.out.println("**** CLEARED ****");
    }

    @Override
    public void createProject() {
        System.out.println("**** PROJECT CREATE ****");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("**** FAULT ****");
        else System.out.println("**** PROJECT CREATED ****");
    }

}
