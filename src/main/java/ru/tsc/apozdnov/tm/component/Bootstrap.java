package ru.tsc.apozdnov.tm.component;

import ru.tsc.apozdnov.tm.api.controller.ICommandController;
import ru.tsc.apozdnov.tm.api.controller.IProjectController;
import ru.tsc.apozdnov.tm.api.controller.ITaskController;
import ru.tsc.apozdnov.tm.api.repository.ICommandRepository;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.ICommandService;
import ru.tsc.apozdnov.tm.api.service.IProjectService;
import ru.tsc.apozdnov.tm.api.service.ITaskService;
import ru.tsc.apozdnov.tm.constant.ArgumentConstant;
import ru.tsc.apozdnov.tm.constant.TerminalConstant;
import ru.tsc.apozdnov.tm.controller.CommandController;
import ru.tsc.apozdnov.tm.controller.ProjectController;
import ru.tsc.apozdnov.tm.controller.TaskController;
import ru.tsc.apozdnov.tm.repository.CommandRepository;
import ru.tsc.apozdnov.tm.repository.ProjectRepository;
import ru.tsc.apozdnov.tm.repository.TaskRepository;
import ru.tsc.apozdnov.tm.service.CommandService;
import ru.tsc.apozdnov.tm.service.ProjectService;
import ru.tsc.apozdnov.tm.service.TaskService;
import ru.tsc.apozdnov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private IProjectController projectController = new ProjectController(projectService);

    public static void close() {
        System.exit(0);
    }

    public void run(final String[] args) {
        if (processArgumentTask(args)) System.exit(0);
        commandController.showWelcome();
        while (true) {
            System.out.println("Enter command:");
            final String cmd = TerminalUtil.nextLine();
            processCommandTask(cmd);
        }
    }

    public void processCommandTask(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConstant.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConstant.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConstant.VERSION:
                commandController.showVersion();
                break;
            case TerminalConstant.HELP:
                commandController.showHelp();
                break;
            case TerminalConstant.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConstant.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConstant.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConstant.EXIT:
                close();
                break;
            default:
                commandController.showFaultCommnand(command);
                break;
        }
    }

    public void processArgumentTask(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConstant.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showFaultArgument(arg);
                break;
        }
    }

    public boolean processArgumentTask(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgumentTask(arg);
        return true;
    }

}
